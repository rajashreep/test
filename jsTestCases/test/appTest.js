const assert = require('chai').assert;
//const app = require('../app');
const fun1 = require('../app').fun1;
describe('App', function(){
	it('fun1 should return connexis', function(){
		let result = fun1();
		assert.equal(result, 'connexis');
	});
	
	it('fun1 should return type string', function(){
		let result = fun1();
		assert.typeOf(result, 'string');
	});
});

