const mongoose = require('mongoose');

const UserData = mongoose.Schema({
	
	user_name:{
		
		type: String,
		required: true
	},
	followers:{
		type: Number,
		required: true
	},
	
})

const Contact = module.exports = mongoose.model('Users', UserData);