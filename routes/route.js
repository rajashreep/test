const express = require('express');
const router = express.Router();

const Contact = require('../models/contacts');
const Data = require('../models/userdata');


//fetching data from userdata
router.get('/datas',(req,res,next)=>{
	
	Data.find(function(err, datas){
		res.json(datas);
	})
})

//adding data to userdata
router.post('/data',(req,res,next)=>{
	
	let newUser = new Data({
		user_name: req.body.user_name,
		followers: req.body.followers
		
	});
	
	newUser.save((err,contact)=>{
		if(err)
		{
			res.json({msg: 'Failed to add user'});
		}
		else{
			res.json({msg: 'User added successfully'});
		}
	});
	
});



//fetching data
router.get('/contacts',(req,res,next)=>{
	
	Contact.find(function(err, contacts){
		res.json(contacts);
	})
	
});
//adding data
router.post('/contact',(req,res,next)=>{
	
	let newContact = new Contact({
		first_name: req.body.first_name,
		last_name: req.body.last_name,
		phone: req.body.phone
		
	});
	
	newContact.save((err,contact)=>{
		if(err)
		{
			res.json({msg: 'Failed to add contact'});
		}
		else{
			res.json({msg: 'Contact added successfully'});
		}
	});
	
});



module.exports = router;