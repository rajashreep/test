
###For Mongodb and Node###

##Development mongo server

> Open Terminal as administrator and Run `mongod` for mongo server. It will connect to the `http://
  localhost:27017/`.

##Development node server

> Run `node app.js` or `node app` for a mongo and node server. Navigate to `http://localhost:3000/`.



###For Angular Client ###

## Development server

> Run `npm start` or `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will    automatically reload if you change any of the source files.

## Code scaffolding

> Run `ng generate component component-name` to generate a new component. You can also use `ng generate 
  directive|pipe|service|class|guard|interface|enum|module`.

## Build

> Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

> Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

> Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

> To get more help on the Angular CLI use `ng help`.


