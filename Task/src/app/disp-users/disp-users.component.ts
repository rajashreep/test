import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {FormsModule, ReactiveFormsModule, NgForm} from '@angular/forms';
import {Http, Response} from '@angular/http';

import 'rxjs/add/operator/map'; 

import {IUserData} from '../iuser-data';
import { UserServiceService } from '../user-service.service';

@Component({
  selector: 'app-disp-users',
  templateUrl: './disp-users.component.html',
  styles: [`
	input.ng-invalid {border-left: 5px solid red;}
	input.ng-valid {border-left: 5px solid green;}
  `]
})
export class DispUsersComponent implements OnInit {

	public myForm: FormGroup;
	public flag: boolean = false;
	public data: any[]; 
	public val: string;
	public routeParam: string;
	users: IUserData;
	post: any;
	name:string = '';
	
  constructor(private _userData: UserServiceService) {}

  ngOnInit() {
	  this.myForm = new FormGroup({
		  'name' : new FormControl(null, Validators.required)
	})
  }
  
  onSave(post)
  {
	this.name = post.name;
	this._userData.getUsers(this.name)
	.subscribe((usersD)=> this.users = usersD);
	if(this.users)
	{
		this.data = this.users.items;
		if(this.data)
		{
			this.flag = true;
		}
	}
	
	
	
			
	}
	  
  }


