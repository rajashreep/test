import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {FormsModule, ReactiveFormsModule, NgForm} from '@angular/forms';
import {Router } from '@angular/router';

@Component({
  selector: 'app-authentication-comp',
  templateUrl: './authentication-comp.component.html',
  styles: [`
	input.ng-invalid {border-left: 5px solid red;}
	input.ng-valid {border-left: 5px solid green;}
  `]
})
export class AuthenticationCompComponent implements OnInit {

	public myForm: FormGroup;
	userId: string = '';
	pass : string = '';
	post : any;
	errMsg: string = '';
	disp : boolean = false;
  
	constructor( private route: Router) {  }

	ngOnInit() {
	  
		this.myForm = new FormGroup({
		  'userId' : new FormControl('', Validators.required),
		  'pass' : new FormControl('', Validators.required)
		})
	}
  
	onSave(post)
	{
	 this.userId = post.userId;
	  this.pass = post.pass;
	  
	  console.log(this.userId);
	  
	  if(this.userId == 'admin' && this.pass == 'admin' )
	  { 
		  this.route.navigate(['/dispUsers']);
	  }
	  else{
		  this.disp = true;
		  this.errMsg = 'Login credentials incorrect';
	  }
	}

}
