import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthenticationCompComponent } from './authentication-comp.component';

describe('AuthenticationCompComponent', () => {
  let component: AuthenticationCompComponent;
  let fixture: ComponentFixture<AuthenticationCompComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthenticationCompComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthenticationCompComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
