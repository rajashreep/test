import { IData } from './idata';

export interface IUserData {
	total_count: number;
	incomplete_results: boolean;
	items: IData [];
}
