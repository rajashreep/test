import { Injectable } from '@angular/core';
import {IUserData} from './iuser-data';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { IuserFollowers } from './iuser-followers';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
@Injectable()
export class UserServiceService {
	constructor(private _http: Http) { }
	
	getUsers(user: string): Observable<IUserData>{
	
	 return this._http.get("http://api.github.com/search/users?q="+ user)
					.map((response: Response)=> <IUserData>response.json())
					.catch(this.handleError)
		
	}
	
	getUserFollowers(loginD: string): Observable<IuserFollowers>{
		
		return this._http.get("https://api.github.com/users/"+loginD)
					.map((response: Response)=> <IuserFollowers>response.json())
					.catch(this.handleError)
		
	}
	
	handleError(error: Response)
	{
		console.error(error);
		return Observable.throw(error);
	}
}
