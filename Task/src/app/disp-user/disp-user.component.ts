import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {Http, Response} from '@angular/http';
import { UserServiceService } from '../user-service.service';
import {IuserFollowers} from '../iuser-followers';

import { UserDBServiceService } from '../user-dbservice.service';
import { User } from '../user';

@Component({
  selector: 'app-disp-user',
  templateUrl: './disp-user.component.html',
  styleUrls: ['./disp-user.component.css']
})
export class DispUserComponent implements OnInit {

	login: string;
	followers: IuserFollowers;
	statusMessage: string = 'Loading data..Please wait..';
	public data: any[];
	
	
	
	users: User[];
	user: User;
	
	uname: string;
	ufollowers: number;
	
	offB: boolean = false;
	
	
  constructor(private route: ActivatedRoute, private _followersData: UserServiceService, private _dbS: UserDBServiceService) {  }

  ngOnInit() {
	  this.login = this.route.snapshot.params.login;
	  
	  this._followersData.getUserFollowers(this.login)
		.subscribe((usersD) => this.followers = usersD);
		
		
	}
	
	saveData(){
	 
		this.uname = this.followers.name;
		this.ufollowers = this.followers.followers;
		
		const newUser ={
			user_name: this.uname,
			followers: this.ufollowers
		}
		
		/*this._dbS.addUser(newUser)
		 .subscribe(data =>{
			 this.users.push(data);
			 
		 });*/
		 
		 this._dbS.addUser(newUser)
		 .subscribe(data =>{
			 this.user = data;
		 })
	
			this.offB = true;
			
	}
}