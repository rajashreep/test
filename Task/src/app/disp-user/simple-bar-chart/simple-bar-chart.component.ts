import { Component, OnInit, Input, ViewEncapsulation , OnChanges } from '@angular/core';
import {IuserFollowers} from '../../iuser-followers';

import { User } from '../../user';
import { UserDBServiceService } from '../../user-dbservice.service';



@Component({
  selector: 'app-simple-bar-chart',
   templateUrl: './simple-bar-chart.component.html',
  styleUrls: ['../../../../node_modules/nvd3/build/nv.d3.css'],
  encapsulation: ViewEncapsulation.None
})

export class SimpleBarChartComponent implements OnInit {
	
  //chart variables	
  options;
  data;
  
  //api variables
  users: User[];
  user: User;
   labs = [];
  ourData = [];
  finalData = [];
  flag: boolean = false;
  constructor(private _dB : UserDBServiceService) { }
  
  ngOnInit() {
	  
	  //data retriving from api
	   this._dB.getUsersDetails()
			.subscribe(users =>
	         this.users = users);
			 
			 
	  
 
	  
	  
	// chart code starts here  
    /*this.options = {
      chart: {
        type: 'discreteBarChart',
        height: 450,
       /* margin : {
          top: 20,
          right: 20,
          bottom: 50,
          left: 55
        },
        x: function(d){return d.label;},
        y: function(d){return d.value;},
        showValues: true,
        valueFormat: function(d){
          return d3.format(',.4f')(d);
        },
        duration: 500,
        xAxis: {
          axisLabel: 'X Axis'
        },
        yAxis: {
          axisLabel: 'Y Axis',
          axisLabelDistance: -10
        }
      }
    }
    this.data = [
      {
        key: "Cumulative Return",
        values: [
          {
            "label" : "A" ,
            "value" : -29.765957771107
          } ,
          {
            "label" : "B" ,
            "value" : 0
          } ,
          {
            "label" : "C" ,
            "value" : 32.807804682612
          } ,
          {
            "label" : "D" ,
            "value" : 196.45946739256
          } ,
          {
            "label" : "E" ,
            "value" : 0.19434030906893
          } ,
          {
            "label" : "F" ,
            "value" : -98.079782601442
          } ,
          {
            "label" : "G" ,
            "value" : -13.925743130903
          } ,
          {
            "label" : "H" ,
            "value" : -5.1387322875705
          }
        ]
      }
    ]; */
	//chart coding ends here
  }
 
 
	drawChart()
	{
		
		this.flag = true;
		
		if(this.users)
		{
			
			 for(var i=0; i<this.users.length; i++){
				 this.user = this.users[i];
				 
				 this.labs[i] = this.user.user_name;
				 this.ourData[i] = this.user.followers;
				 
			 }
			
			for(var i=0; i< this.labs.length; i++)
			{
				this.finalData.push({
					label: this.labs[i], 
					value: this.ourData[i]
				})
				
			}
			console.log(this.finalData);
		}
		
		//chart code start
		 this.options = {
      chart: {
        type: 'discreteBarChart',
        height: 450,
       /* margin : {
          top: 20,
          right: 20,
          bottom: 50,
          left: 55
        },*/
        x: function(d){return d.label;},
        y: function(d){return d.value;},
        showValues: true,
        valueFormat: function(d){
          return d3.format(',.4f')(d);
        },
        duration: 500,
        xAxis: {
          axisLabel: 'X Axis'
        },
        yAxis: {
          axisLabel: 'Y Axis',
          axisLabelDistance: -10
        }
      }
    }
    this.data = 
	[
      {
        key: "Cumulative Return",
        values: this.finalData 
		/*
		 [
          {
            "label" : "A" ,
            "value" : -29.765957771107
          } ,
          {
            "label" : "B" ,
            "value" : 0
          } ,
          {
            "label" : "C" ,
            "value" : 32.807804682612
          } ,
          {
            "label" : "D" ,
            "value" : 196.45946739256
          } ,
          {
            "label" : "E" ,
            "value" : 0.19434030906893
          } ,
          {
            "label" : "F" ,
            "value" : -98.079782601442
          } ,
          {
            "label" : "G" ,
            "value" : -13.925743130903
          } ,
          {
            "label" : "H" ,
            "value" : -5.1387322875705
          }
        ]*/
      }
    ];
	//chart code ends	
	}
}