import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { User } from './user';
import 'rxjs/add/operator/map';

@Injectable()
export class UserDBServiceService {

  constructor(private http: Http) { }
	
	//retriving user details
	
	getUsersDetails()
	{
		return this.http.get('http://localhost:3000/api/datas')
		.map(res => res.json());
	}
  
	//add contacts
	
	addUser(newUser)
	{
		
		var headers = new Headers();
		headers.append('Content-Type', 'application/json');
		return this.http.post('http://localhost:3000/api/data', newUser,{headers: headers})
		.map(res => res.json());
	}
	
  

}
