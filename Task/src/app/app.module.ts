import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpModule } from '@angular/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { AppComponent } from './app.component';
import { AuthenticationCompComponent } from './authentication-comp/authentication-comp.component';
import { Routes, RouterModule } from '@angular/router';
import { DispUsersComponent } from './disp-users/disp-users.component';

import { UserServiceService } from './user-service.service';
import { DispUserComponent } from './disp-user/disp-user.component';
import { SimpleBarChartComponent } from './disp-user/simple-bar-chart/simple-bar-chart.component';
import { PageNotFoundComponent} from './Others/pageNotFound.component';

import { UserDBServiceService } from './user-dbservice.service';
import { NvD3Module } from 'ng2-nvd3';


import 'd3';
import 'nvd3';
import { BarGraphComponent } from './disp-user/bar-graph/bar-graph.component';
const routes: Routes = [

{ path: 'login', component: AuthenticationCompComponent },

{ path: 'dispUsers', component: DispUsersComponent},

{ path: 'user/:login', component: DispUserComponent},

{ path: 'followers', component: SimpleBarChartComponent},
{ path: '',  redirectTo: 'login', pathMatch: 'full' },
{ path:'**' , component: PageNotFoundComponent}

];


@NgModule({
  declarations: [
    AppComponent,
    AuthenticationCompComponent,
    DispUsersComponent,
    DispUserComponent,
    BarGraphComponent,
    
	SimpleBarChartComponent,
	PageNotFoundComponent
   ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
	NvD3Module,
	ReactiveFormsModule,
	
	RouterModule.forRoot(routes)
  ],
  providers: [UserServiceService,UserDBServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
