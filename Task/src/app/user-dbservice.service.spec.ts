import { TestBed, inject } from '@angular/core/testing';

import { UserDBServiceService } from './user-dbservice.service';

describe('UserDBServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserDBServiceService]
    });
  });

  it('should be created', inject([UserDBServiceService], (service: UserDBServiceService) => {
    expect(service).toBeTruthy();
  }));
});
